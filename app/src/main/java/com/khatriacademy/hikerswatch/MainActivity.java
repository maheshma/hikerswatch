package com.khatriacademy.hikerswatch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.Manifest.permission;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity {
    LocationManager locationManager;
    LocationListener locationListener;


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
           startListening();
        }
    }
    public void updateLocationInfo(Location location){
        Log.i("LocationInfo:", location.toString());
        TextView longTextView=(TextView)findViewById(R.id.longitude);
        TextView latTextView=(TextView)findViewById(R.id.lattitude);
        TextView accTextView=(TextView)findViewById(R.id.accurayTextview);
        TextView altTextView=(TextView)findViewById(R.id.altTextview);

        longTextView.setText("Longitude:"+location.getLongitude());
        latTextView.setText("Lattitude:"+location.getLatitude());
        accTextView.setText("Accuracy:"+location.getAccuracy());
        altTextView.setText("Altitude:"+location.getAltitude());
        Geocoder geocoder=new Geocoder(getApplicationContext(), Locale.getDefault());
        try {

            String address="Could not find the Address";
            List<Address> addressList=geocoder.getFromLocation(location.getLatitude(),location.getLongitude(),1);
            if(addressList!=null && addressList.size() > 0) {
                address="Address:\n";
                Log.i("placeinfo:", addressList.get(0).toString());
                if(addressList.get(0).getSubThoroughfare()!=null){
                    address+=addressList.get(0).getSubThoroughfare()+ "";
                }
                if(addressList.get(0).getThoroughfare()!=null){
                    address+=addressList.get(0).getThoroughfare()+ "\n";
                }
                if(addressList.get(0).getLocality()!=null){
                    address+=addressList.get(0).getLocality()+ "\n";
                }
                if(addressList.get(0).getPostalCode()!=null){
                    address+=addressList.get(0).getPostalCode()+ "\n";
                }
                if(addressList.get(0).getCountryName()!=null){
                    address+=addressList.get(0).getCountryName()+"\n";
                }
            }

            TextView addrTextView=(TextView)findViewById(R.id.addresssTextview);
            addrTextView.setText(address);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void startListening(){
        if(ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        }
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
               updateLocationInfo(location);
            }
        };
        if (Build.VERSION.SDK_INT < 23) {
            if (ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
           startListening();
        }
        else{
            if(ContextCompat.checkSelfPermission(this,ACCESS_FINE_LOCATION) != getPackageManager().PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this,new String[]{ACCESS_FINE_LOCATION},1);

            }else{
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,locationListener);
                Location location=locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(location!=null){
                    updateLocationInfo(location);
                }

            }
        }
    }
}